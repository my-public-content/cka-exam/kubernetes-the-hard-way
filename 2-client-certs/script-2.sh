#!/bin/bash

init () {

mkdir -p admin-client
cat > admin-csr.json << EOF
{
  "CN": "admin",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Portland",
      "O": "system:masters",
      "OU": "Kubernetes The Hard Way",
      "ST": "Oregon"
    }
  ]
}
EOF

cfssl gencert \
  -ca=./ca/ca.pem \
  -ca-key=./ca/ca-key.pem \
  -config=./ca/ca-config.json \
  -profile=kubernetes \
admin-csr.json | cfssljson -bare admin

mv *.json admin-client
mv *.pem admin-client
mv admin.csr admin-client


cat > ${1}-csr.json << EOF
{
  "CN": "system:node:${1}",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Portland",
      "O": "system:nodes",
      "OU": "Kubernetes The Hard Way",
      "ST": "Oregon"
    }
  ]
}
EOF

cfssl gencert \
  -ca=./ca/ca.pem \
  -ca-key=./ca/ca-key.pem \
  -config=./ca/ca-config.json \
  -hostname=${1},${2} \
  -profile=kubernetes \
  ${1}-csr.json | cfssljson -bare ${1}

cat > ${3}-csr.json << EOF
{
  "CN": "system:node:${3}",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Portland",
      "O": "system:nodes",
      "OU": "Kubernetes The Hard Way",
      "ST": "Oregon"
    }
  ]
}
EOF

cfssl gencert \
  -ca=./ca/ca.pem \
  -ca-key=./ca/ca-key.pem \
  -config=./ca/ca-config.json \
  -hostname=${3},${4} \
  -profile=kubernetes \
  ${3}-csr.json | cfssljson -bare ${3}

mkdir worker-1
mkdir worker-2

mv ${1}* worker-1
mv ${3}* worker-2

cat > kube-controller-manager-csr.json << EOF
{
  "CN": "system:kube-controller-manager",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Portland",
      "O": "system:kube-controller-manager",
      "OU": "Kubernetes The Hard Way",
      "ST": "Oregon"
    }
  ]
}
EOF

cfssl gencert \
  -ca=./ca/ca.pem \
  -ca-key=./ca/ca-key.pem \
  -config=./ca/ca-config.json \
  -profile=kubernetes \
  kube-controller-manager-csr.json | cfssljson -bare kube-controller-manager

mkdir -p kube-controller-manager
mv kube-controller-manager* kube-controller-manager

cat > kube-proxy-csr.json << EOF
{
  "CN": "system:kube-proxy",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Portland",
      "O": "system:node-proxier",
      "OU": "Kubernetes The Hard Way",
      "ST": "Oregon"
    }
  ]
}
EOF

cfssl gencert \
  -ca=./ca/ca.pem \
  -ca-key=./ca/ca-key.pem \
  -config=./ca/ca-config.json \
  -profile=kubernetes \
  kube-proxy-csr.json | cfssljson -bare kube-proxy

mkdir -p kube-proxy
mv kube-proxy* kube-proxy

cat > kube-scheduler-csr.json << EOF
{
  "CN": "system:kube-scheduler",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Portland",
      "O": "system:kube-scheduler",
      "OU": "Kubernetes The Hard Way",
      "ST": "Oregon"
    }
  ]
}
EOF

cfssl gencert \
  -ca=./ca/ca.pem \
  -ca-key=./ca/ca-key.pem \
  -config=./ca/ca-config.json \
  -profile=kubernetes \
  kube-scheduler-csr.json | cfssljson -bare kube-scheduler

mkdir -p kube-scheduler
mv kube-scheduler* kube-scheduler

}

"$@"