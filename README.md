## 1 - use init script
```bash
sudo chmod +x init.sh

init.sh ca-certs <cfssl version>
init.sh clients-certs <worker nodes hostname and ip>
init.sh kube-api-certs <master node hostname and ip and authorized ips or hostnames to call api>
init.sh sa-certs
init.sh scp
```

## 2 - copy files
- Copy files in scp-to-worker-<number> to corresponding worker node
- Copy files in scp-to-masters to all master nodes

