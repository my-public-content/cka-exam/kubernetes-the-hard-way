#!/bin/bash

mkdir -p scp-to-worker-1
mkdir -p scp-to-worker-2
mkdir -p scp-to-masters

for i in scp-to-worker-1 scp-to-worker-2 scp-to-masters; do cp ./ca/ca.pem $i; done

for i in 1 2; do cp ./worker-$i/*.pem scp-to-worker-$i; done

cp ./ca/ca-key.pem scp-to-masters
cp ./kubernetes-api/*.pem scp-to-masters
cp ./service-account/*.pem scp-to-masters

"$@"