#!/bin/bash

# arg1 cfssl version
ca-certs () {

mkdir -p ca

curl -s -L -o /bin/cfssl https://github.com/cloudflare/cfssl/releases/download/v${1}/cfssl_${1}_linux_amd64
curl -s -L -o /bin/cfssljson https://github.com/cloudflare/cfssl/releases/download/v${1}/cfssljson_${1}_linux_amd64
curl -s -L -o /bin/cfssl-certinfo https://github.com/cloudflare/cfssl/releases/download/v${1}/cfssl-certinfo_${1}_linux_amd64

chmod +x /bin/cfssl*

cat > ca-config.json << EOF
{
  "signing": {
    "default": {
      "expiry": "8760h"
    },
    "profiles": {
      "kubernetes": {
        "usages": ["signing", "key encipherment", "server auth", "client auth"],
        "expiry": "8760h"
      }
    }
  }
}
EOF

cat > ca-csr.json << EOF
{
  "CN": "Kubernetes",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Portland",
      "O": "Kubernetes",
      "OU": "CA",
      "ST": "Oregon"
    }
  ]
}
EOF

cfssl gencert -initca ca-csr.json | cfssljson -bare ca

mv *.json ca
mv *.pem ca
mv ca.csr ca
}


# arg1 worker-1 hostname
# arg2 worker-2 ip
# arg3 worker-3 hostname
# arg4 worker-4 ip
clients-certs () {

mkdir -p admin-client
cat > admin-csr.json << EOF
{
  "CN": "admin",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Portland",
      "O": "system:masters",
      "OU": "Kubernetes The Hard Way",
      "ST": "Oregon"
    }
  ]
}
EOF

cfssl gencert \
  -ca=./ca/ca.pem \
  -ca-key=./ca/ca-key.pem \
  -config=./ca/ca-config.json \
  -profile=kubernetes \
admin-csr.json | cfssljson -bare admin

mv *.json admin-client
mv *.pem admin-client
mv admin.csr admin-client


cat > ${1}-csr.json << EOF
{
  "CN": "system:node:${1}",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Portland",
      "O": "system:nodes",
      "OU": "Kubernetes The Hard Way",
      "ST": "Oregon"
    }
  ]
}
EOF

cfssl gencert \
  -ca=./ca/ca.pem \
  -ca-key=./ca/ca-key.pem \
  -config=./ca/ca-config.json \
  -hostname=${1},${2} \
  -profile=kubernetes \
  ${1}-csr.json | cfssljson -bare ${1}

cat > ${3}-csr.json << EOF
{
  "CN": "system:node:${3}",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Portland",
      "O": "system:nodes",
      "OU": "Kubernetes The Hard Way",
      "ST": "Oregon"
    }
  ]
}
EOF

cfssl gencert \
  -ca=./ca/ca.pem \
  -ca-key=./ca/ca-key.pem \
  -config=./ca/ca-config.json \
  -hostname=${3},${4} \
  -profile=kubernetes \
  ${3}-csr.json | cfssljson -bare ${3}

mkdir worker-1
mkdir worker-2

mv ${1}* worker-1
mv ${3}* worker-2

cat > kube-controller-manager-csr.json << EOF
{
  "CN": "system:kube-controller-manager",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Portland",
      "O": "system:kube-controller-manager",
      "OU": "Kubernetes The Hard Way",
      "ST": "Oregon"
    }
  ]
}
EOF

cfssl gencert \
  -ca=./ca/ca.pem \
  -ca-key=./ca/ca-key.pem \
  -config=./ca/ca-config.json \
  -profile=kubernetes \
  kube-controller-manager-csr.json | cfssljson -bare kube-controller-manager

mkdir -p kube-controller-manager
mv kube-controller-manager* kube-controller-manager

cat > kube-proxy-csr.json << EOF
{
  "CN": "system:kube-proxy",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Portland",
      "O": "system:node-proxier",
      "OU": "Kubernetes The Hard Way",
      "ST": "Oregon"
    }
  ]
}
EOF

cfssl gencert \
  -ca=./ca/ca.pem \
  -ca-key=./ca/ca-key.pem \
  -config=./ca/ca-config.json \
  -profile=kubernetes \
  kube-proxy-csr.json | cfssljson -bare kube-proxy

mkdir -p kube-proxy
mv kube-proxy* kube-proxy

cat > kube-scheduler-csr.json << EOF
{
  "CN": "system:kube-scheduler",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Portland",
      "O": "system:kube-scheduler",
      "OU": "Kubernetes The Hard Way",
      "ST": "Oregon"
    }
  ]
}
EOF

cfssl gencert \
  -ca=./ca/ca.pem \
  -ca-key=./ca/ca-key.pem \
  -config=./ca/ca-config.json \
  -profile=kubernetes \
  kube-scheduler-csr.json | cfssljson -bare kube-scheduler

mkdir -p kube-scheduler
mv kube-scheduler* kube-scheduler

}

#arg1 hostnames or ips authorized to call api
kube-api-certs () {

cat > kubernetes-csr.json << EOF
{

  "CN": "kubernetes",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
   {
      "C": "US",
      "L": "Portland",
      "O": "Kubernetes",
      "OU": "Kubernetes The Hard Way",
      "ST": "Oregon"
    }
  ]
}
EOF

cfssl gencert \
  -ca=./ca/ca.pem \
  -ca-key=./ca/ca-key.pem \
  -config=./ca/ca-config.json \
  -hostname=kubernetes.default,10.32.0.1,127.0.0.1,localhost,${1} \
  -profile=kubernetes \
  kubernetes-csr.json | cfssljson -bare kubernetes

mkdir kubernetes-api
mv kubernetes* kubernetes-api

}

sa-certs () {

cat > service-account-csr.json << EOF
{
  "CN": "service-accounts",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Portland",
      "O": "Kubernetes",
      "OU": "Kubernetes The Hard Way",
      "ST": "Oregon"
    }
  ]
}
EOF

cfssl gencert \
  -ca=./ca/ca.pem \
  -ca-key=./ca/ca-key.pem \
  -config=./ca/ca-config.json \
  -profile=kubernetes \
  service-account-csr.json | cfssljson -bare service-account

mkdir -p service-account
mv service-account* service-account

}

scp () {
mkdir -p scp-to-worker-1
mkdir -p scp-to-worker-2
mkdir -p scp-to-masters

for i in scp-to-worker-1 scp-to-worker-2 scp-to-masters; do cp ./ca/ca.pem $i; done

for i in 1 2; do cp ./worker-$i/*.pem scp-to-worker-$i; done

cp ./ca/ca-key.pem scp-to-masters
cp ./kubernetes-api/*.pem scp-to-masters
cp ./service-account/*.pem scp-to-masters
}

"$@"