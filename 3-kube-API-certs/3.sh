#!/bin/bash

init () {

cat > kubernetes-csr.json << EOF
{

  "CN": "kubernetes",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
   {
      "C": "US",
      "L": "Portland",
      "O": "Kubernetes",
      "OU": "Kubernetes The Hard Way",
      "ST": "Oregon"
    }
  ]
}
EOF

cfssl gencert \
  -ca=./ca/ca.pem \
  -ca-key=./ca/ca-key.pem \
  -config=./ca/ca-config.json \
  -hostname=kubernetes.default,10.32.0.1,127.0.0.1,localhost,${1} \
  -profile=kubernetes \
  kubernetes-csr.json | cfssljson -bare kubernetes

mkdir kubernetes-api
mv kubernetes* kubernetes-api

}



"$@"

