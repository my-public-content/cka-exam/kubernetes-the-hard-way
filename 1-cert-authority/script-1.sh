#!/bin/bash

mkdir -p ca

# arg1 cfssl version
init () {
curl -s -L -o /bin/cfssl https://github.com/cloudflare/cfssl/releases/download/v${1}/cfssl_${1}_linux_amd64
curl -s -L -o /bin/cfssljson https://github.com/cloudflare/cfssl/releases/download/v${1}/cfssljson_${1}_linux_amd64
curl -s -L -o /bin/cfssl-certinfo https://github.com/cloudflare/cfssl/releases/download/v${1}/cfssl-certinfo_${1}_linux_amd64

chmod +x /bin/cfssl*

cat > ca-config.json << EOF
{
  "signing": {
    "default": {
      "expiry": "8760h"
    },
    "profiles": {
      "kubernetes": {
        "usages": ["signing", "key encipherment", "server auth", "client auth"],
        "expiry": "8760h"
      }
    }
  }
}
EOF

cat > ca-csr.json << EOF
{
  "CN": "Kubernetes",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Portland",
      "O": "Kubernetes",
      "OU": "CA",
      "ST": "Oregon"
    }
  ]
}
EOF

cfssl gencert -initca ca-csr.json | cfssljson -bare ca

mv *.json ca
mv *.pem ca
mv ca.csr ca
}

"$@"